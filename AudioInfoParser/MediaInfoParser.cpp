// AudioInfoParser.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "AudioInfoLenth.h"
#include "MetaDataPlayer.h"
#include "MediaInfoParser.h"
#include "AudioDecoder.h"
#include "VideoDecoder.h"
#include "MetaDataPlayer.h"

#define  _PCM  0
#define  _MediaInfo 0
#define  _YUV 0
#define  _PCMPLAYER 0
#define  _YUVPLAYER 1

int main(int argc, char* argv[])
{
#if _MediaInfo
	Decode_MediaInfo::Audio_HEADER iInfo;
	Decode_MediaInfo::AudioMetaInfo iMetaData;
	Decode_MediaInfo::ParseAudioInfo("xiaoqingge.mp3", iInfo, &iMetaData);
	Decode_MediaInfo::Video_HEADER iVideoInfo;
	Decode_MediaInfo::ParseVideoInfo("Titanic.mkv", iVideoInfo);
#endif // _MediaInfo

#if _PCM
	Decoder::AudioDecoder iPlay;
	iPlay.LoadAudio("yuandian.flv");
	iPlay.SavePcm("xiaoqingge.pcm");
#endif // _PCM


#if _YUV
    Decoder::VideoDecoder iVideoLoader;
    iVideoLoader.Converter2YUV("yuandian.flv");

#endif

#if _PCMPLAYER
    Player::PCMPlayer iPCMPlayer;
    iPCMPlayer.PlayPCM("xiaoqingge.pcm");
#endif

#if _YUVPLAYER
    Player::YUVPlayer iYUVPlayer;
    iYUVPlayer.PlayYUV("yuandian.flv.yuv", 512, 288);
#endif



    system("pause");
    return 0;
}
