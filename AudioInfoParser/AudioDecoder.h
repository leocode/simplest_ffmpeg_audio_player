#pragma once
#include <string>
using namespace  std;


namespace Decoder
{
	class AudioDecoder
	{
		typedef unsigned char uint8;
	public:
		AudioDecoder();
		~AudioDecoder();
		int  LoadAudio(const string & tFilename);
		bool SavePcm(const string &tFilename);
		long GetPcmSize();

	private:
		std::string m_strBuff;
	};

}