#include "stdafx.h"
#include "AudioDecoder.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include < windows.h> 
#define __STDC_CONSTANT_MACROS

#ifdef _WIN32
//Windows
extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswresample/swresample.h"
#include "SDL2/SDL.h"
};
#endif


#define MAX_AUDIO_FRAME_SIZE 192000 // 1 second of 48khz 32bit audio

#define PIRNT_PROCESS 0
#define _TIME 0



namespace Decoder
{

	AudioDecoder::AudioDecoder()
	{
	}

	AudioDecoder::~AudioDecoder()
	{
	}


	int AudioDecoder::LoadAudio(const string & tFilename)
	{

#if _TIME
		long iBeforTick = GetTickCount();
#endif // _TIME

		m_strBuff.clear();
		av_register_all();
		avformat_network_init();


		if (tFilename.empty())
		{
			return false;
		}
		AVFormatContext	*pFormatCtx;
		int				i, audioStream;
		AVCodecContext	*pCodecCtx;
		AVCodec			*pCodec;
		AVPacket		*packet;
		uint8_t			*out_buffer;
		AVFrame			*pFrame;
		int ret;
		uint32_t len = 0;
		int got_picture = 1;
		int index = 0;
		int64_t in_channel_layout;
		struct SwrContext *au_convert_ctx;
		FILE *pFile = NULL;

		av_register_all();
		avformat_network_init();
		pFormatCtx = avformat_alloc_context();
		//Open
		if (avformat_open_input(&pFormatCtx, tFilename.c_str(), NULL, NULL) != 0) {
			printf("Couldn't open input stream.\n");
			return -1;
		}
		// Retrieve stream information
		if (avformat_find_stream_info(pFormatCtx, NULL) < 0) {
			printf("Couldn't find stream information.\n");
			return -1;
		}
		// Dump valid information onto standard error
		av_dump_format(pFormatCtx, 0, tFilename.c_str(), false);
		// Find the first audio stream
		audioStream = -1;
		for (i = 0; i < pFormatCtx->nb_streams; i++)
			if (pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO) {
				audioStream = i;
				break;
			}
		if (audioStream == -1) {
			printf("Didn't find a audio stream.\n");
			return -1;
		}

		// Get a pointer to the codec context for the audio stream
		pCodecCtx = pFormatCtx->streams[audioStream]->codec;

		// Find the decoder for the audio stream
		pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
		if (pCodec == NULL) {
			printf("Codec not found.\n");
			return -1;
		}

		// Open codec
		if (avcodec_open2(pCodecCtx, pCodec, NULL) < 0) {
			printf("Could not open codec.\n");
			return -1;
		}


		packet = (AVPacket *)av_malloc(sizeof(AVPacket));
		av_init_packet(packet);
		//Out Audio Param
		uint64_t out_channel_layout = AV_CH_LAYOUT_STEREO;
		//nb_samples: AAC-1024 MP3-1152
		int out_nb_samples = pCodecCtx->frame_size;
		AVSampleFormat out_sample_fmt = AV_SAMPLE_FMT_S16;
		int out_sample_rate = pCodecCtx->sample_rate;
		int out_channels = av_get_channel_layout_nb_channels(out_channel_layout);
		//Out Buffer Size
		int out_buffer_size = av_samples_get_buffer_size(NULL, out_channels, out_nb_samples, out_sample_fmt, 1);

		out_buffer = (uint8_t *)av_malloc(MAX_AUDIO_FRAME_SIZE * 2);
		pFrame = av_frame_alloc();
		//SDL------------------
		//FIX:Some Codec's Context Information is missing
		in_channel_layout = av_get_default_channel_layout(pCodecCtx->channels);
		//Swr
		au_convert_ctx = swr_alloc();
		au_convert_ctx = swr_alloc_set_opts(au_convert_ctx, out_channel_layout, out_sample_fmt, out_sample_rate,
			in_channel_layout, pCodecCtx->sample_fmt, pCodecCtx->sample_rate, 0, NULL);
		swr_init(au_convert_ctx);

		while (av_read_frame(pFormatCtx, packet) >= 0) {
			if (packet->stream_index == audioStream) {
				ret = avcodec_decode_audio4(pCodecCtx, pFrame, &got_picture, packet);
				if (ret < 0) {
					printf("Error in decoding audio frame.\n");
					return -1;
				}
				if (got_picture > 0) {
					swr_convert(au_convert_ctx, &out_buffer, MAX_AUDIO_FRAME_SIZE, (const uint8_t **)pFrame->data, pFrame->nb_samples);
#if PIRNT_PROCESS
					printf("index:%5d\t pts:%lld\t packet size:%d\n", index, packet->pts, packet->size);
#endif

					//uint8 * iTemp = (uint8 *)realloc(m_pcmBuff, m_pcmSize + out_buffer_size);
					//if (iTemp == NULL)
					//{
					//	continue;
					//}
					//m_pcmBuff = iTemp;
					//memcpy(m_pcmBuff + m_pcmSize, (uint8*)out_buffer, out_buffer_size);
					//m_pcmSize += out_buffer_size;
					m_strBuff += string((char*)out_buffer, out_buffer_size);
					index++;
				}

			}
			av_free_packet(packet);
		}
		swr_free(&au_convert_ctx);

		av_free(out_buffer);
		avcodec_close(pCodecCtx);
		avformat_close_input(&pFormatCtx);
		long iAfterTicks = GetTickCount();
#if _TIME
		printf("cost %d  ms to parse audio", iAfterTicks - iBeforTick);
#endif // _TIME

		return 0;
	}

	bool AudioDecoder::SavePcm(const string &tFilename)
	{
		if (tFilename.empty())
		{
			return false;
		}
		//if (m_pcmSize == 0)
		//{
		//	return false;
		//}
		//FILE * iFileHanle = fopen(tFilename.c_str(), "wb+");
		//fwrite(m_pcmBuff, 1, m_pcmSize, iFileHanle);
		//fclose(iFileHanle);

		if (m_strBuff.empty())
		{
			return false;
		}
		FILE * iFileHanle = fopen(tFilename.c_str(), "wb+");
		fwrite((uint8*)m_strBuff.c_str(), 1, m_strBuff.size(), iFileHanle);
		fclose(iFileHanle);

		return true;
	}

	long AudioDecoder::GetPcmSize()
	{
		return m_strBuff.size();
	}


}