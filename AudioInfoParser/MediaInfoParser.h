
//当前类仅依赖avformat 和 avcodec两个动态库

#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define __STDC_CONSTANT_MACROS
extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
};


#include <string>
#include <vector>
#include <AtlConv.h>


namespace Decode_MediaInfo
{


#pragma region _TYPE
	using namespace  std;
	typedef short int16;
	typedef unsigned int uint;
	typedef unsigned char uint8;
	typedef unsigned short uint16;
	typedef unsigned short ushort;
	typedef unsigned char uchar;
	typedef unsigned int uint32;
	typedef unsigned long  uit64;
#pragma endregion _TYPE


#pragma pack(push, 1)
	struct Audio_HEADER {
		uint16 format; 			//2 bytes  0x0001          Format tag: 1 = PCM
		uint16 channels; 		//2 bytes  <channels>      Channels: 1 = mono, 2 = stereo
		uint32 sampleRate;		//4 bytes  <sample rate>   Samples per second: e.g., 44100
		uint32 bitrate;		//4 bytes  <bytes/second>  sample rate * block align
		uint16 blockAlign;		//2 bytes  <block align>   channels * bits/sample / 8
		uint16 bitsPerSample; 	//2 bytes  <bits/sample>   8 or 16
		uint16 extraBytes;		// for compatible with WAVEFORMATEX struct 
		uit64 durition;
	};

	struct Video_HEADER {
		uit64 durition;

		uint32 width;
		uint32 height;
		uint32 framerate;
		uint32 bitrate;
	};


#pragma pack(pop)
	struct AudioMetaInfo
	{
		wstring artist;
		wstring ablum;
		wstring title;
		wstring date;
		wstring tract;
		wstring comment;
	};


#pragma region _API
	int ParseAudioInfo(const std::string & tFileName, Audio_HEADER & tHeadInfo, AudioMetaInfo* tPtrMusic = NULL);
	int ParseVideoInfo(const std::string & tFileName, Video_HEADER & tHeadInfo);
#pragma endregion _API


#pragma region _ASISTFUNC
	void Utf8ToUnicode(std::string const &src, std::wstring & des_);
	void Utf8ToUnicode(std::string const &src, std::wstring & des_)
	{
		int n = MultiByteToWideChar(CP_ACP, 0, src.c_str(), -1, NULL, 0);
		std::vector<wchar_t> temp(n);
		::MultiByteToWideChar(CP_ACP, 0, src.c_str(), -1, &temp[0], n);
		des_ = &temp[0];
	}
#pragma endregion _ASISTFUNC



	inline int ParseAudioInfo(const std::string & tFileName, Audio_HEADER & tHeadInfo, AudioMetaInfo* tPtrMusic)
	{
		AVFormatContext	*pFormatCtx = nullptr;
		AVCodecContext	*pCodecCtx = nullptr;
		AVCodec			*pCodec;
		int audioStream;
		int ret;
		int64_t in_channel_layout;

		av_register_all();
		avformat_network_init();
		pFormatCtx = avformat_alloc_context();
		//Open
		if (avformat_open_input(&pFormatCtx, tFileName.c_str(), NULL, NULL) != 0) {
			printf("Couldn't open input stream.\n");
			return -1;
		}
		// Retrieve stream information
		if (avformat_find_stream_info(pFormatCtx, NULL) < 0) {
			printf("Couldn't find stream information.\n");
			return -1;
		}
#ifdef _DEBUG
		// Dump valid information onto standard error
		av_dump_format(pFormatCtx, 0, tFileName.c_str(), false);
#endif

		// Find the first audio stream
		audioStream = -1;
		for (int i = 0; i < pFormatCtx->nb_streams; i++)
			if (pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO) {
				audioStream = i;
				break;
			}

		if (audioStream == -1) {
			printf("Didn't find a audio stream.\n");
			return -1;
		}

		// Get a pointer to the codec context for the audio stream
		pCodecCtx = pFormatCtx->streams[audioStream]->codec;

		// Find the decoder for the audio stream
		pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
		if (pCodec == NULL) {
			printf("Codec not found.\n");
			return -1;
		}

		// Open codec
		if (avcodec_open2(pCodecCtx, pCodec, NULL) < 0) {
			printf("Could not open codec.\n");
			return -1;
		}

		tHeadInfo.durition = pFormatCtx->duration;
		tHeadInfo.bitrate = pFormatCtx->bit_rate;
		tHeadInfo.channels = pCodecCtx->channels;
		tHeadInfo.blockAlign = pCodecCtx->block_align;
		tHeadInfo.format = pCodecCtx->codec_id;
		tHeadInfo.sampleRate = pCodecCtx->sample_rate;
		tHeadInfo.extraBytes = pCodecCtx->extradata_size;
		tHeadInfo.bitsPerSample = pCodecCtx->bits_per_raw_sample;

		if (tPtrMusic != nullptr)
		{
			int iCount = av_dict_count(pFormatCtx->metadata);
			AVDictionaryEntry* dictEntry = av_dict_get(pFormatCtx->metadata, "artist", NULL, 0);
			AVDictionaryEntry* dictEntry1 = av_dict_get(pFormatCtx->metadata, "ablum", NULL, 0);
			AVDictionaryEntry* dictEntry2 = av_dict_get(pFormatCtx->metadata, "title", NULL, 0);
			AVDictionaryEntry* dictEntry3 = av_dict_get(pFormatCtx->metadata, "date", NULL, 0);
			AVDictionaryEntry* dictEntry4 = av_dict_get(pFormatCtx->metadata, "date", NULL, 0);

			if (dictEntry != nullptr && dictEntry->value != nullptr)
			{
				Utf8ToUnicode(string(dictEntry->value), tPtrMusic->artist);
			}
			if (dictEntry1 !=nullptr && dictEntry1->value != nullptr)
			{
				Utf8ToUnicode(string(dictEntry1->value), tPtrMusic->ablum);
			}

			if (dictEntry2 != nullptr && dictEntry2->value != nullptr)
			{
				Utf8ToUnicode(string(dictEntry2->value), tPtrMusic->title);
			}
			if (dictEntry3 != nullptr && dictEntry3->value != nullptr)
			{
				Utf8ToUnicode(string(dictEntry3->value), tPtrMusic->date);
			}
			if (dictEntry4 != nullptr && dictEntry4->value != nullptr)
			{
				Utf8ToUnicode(string(dictEntry4->value), tPtrMusic->tract);
			}
		}
		avcodec_close(pCodecCtx);
		avformat_close_input(&pFormatCtx);
		return 0;
	}


	inline int ParseVideoInfo(const std::string & tFileName, Video_HEADER & tHeadInfo)
	{
		AVFormatContext	*pFormatCtx = nullptr;
		AVCodecContext	*pCodecCtx = nullptr;
		AVCodec			*pCodec;
		int audioStream;
		int ret;
		int64_t in_channel_layout;

		av_register_all();
		avformat_network_init();
		pFormatCtx = avformat_alloc_context();
		//Open
		if (avformat_open_input(&pFormatCtx, tFileName.c_str(), NULL, NULL) != 0) {
			printf("Couldn't open input stream.\n");
			return -1;
		}
		// Retrieve stream information
		if (avformat_find_stream_info(pFormatCtx, NULL) < 0) {
			printf("Couldn't find stream information.\n");
			return -1;
		}

#ifdef _DEBUG
		// Dump valid information onto standard error
		av_dump_format(pFormatCtx, 0, tFileName.c_str(), false);
#endif

		// Find the first video stream
		audioStream = -1;
		for (int i = 0; i < pFormatCtx->nb_streams; i++)
			if (pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
				audioStream = i;
				break;
			}

		if (audioStream == -1) {
			printf("Didn't find a video stream.\n");
			return -1;
		}

		// Get a pointer to the codec context for the video stream
		pCodecCtx = pFormatCtx->streams[audioStream]->codec;

		// Find the decoder for the audio stream
		pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
		if (pCodec == NULL) {
			printf("Codec not found.\n");
			return -1;
		}

		// Open codec
		if (avcodec_open2(pCodecCtx, pCodec, NULL) < 0) {
			printf("Could not open codec.\n");
			return -1;
		}

		AVStream *stream = pFormatCtx->streams[audioStream];
		tHeadInfo.framerate = stream->avg_frame_rate.num / stream->avg_frame_rate.den;//每秒多少帧

		tHeadInfo.durition = pFormatCtx->duration;
		tHeadInfo.bitrate = pFormatCtx->bit_rate;

		tHeadInfo.width = pCodecCtx->width;
		tHeadInfo.height = pCodecCtx->height;

		avcodec_close(pCodecCtx);
		avformat_close_input(&pFormatCtx);
		return 0;
	}

}