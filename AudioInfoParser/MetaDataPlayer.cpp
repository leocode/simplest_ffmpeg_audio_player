#include "stdafx.h"
#include "MetaDataPlayer.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include < windows.h> 
#define __STDC_CONSTANT_MACROS

#ifdef _WIN32
//Windows
extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libswresample/swresample.h"
#include "SDL2/SDL.h"
};
#endif



namespace Player
{


#pragma region _AUDIO
#define MAX_AUDIO_FRAME_SIZE 192000 // 1 second of 48khz 32bit audio
#define OUTPUT_PCM 1
#define USE_SDL 0
#define PIRNT_PROCESS 0
#define PCMPARSE_TIME 1


    static  Uint8  *audio_chunk;
    static  Uint32  audio_len;
    static  Uint8  *audio_pos;
    void  fill_audio(void *udata, Uint8 *stream, int len) {
        //SDL 2.0
        SDL_memset(stream, 0, len);
        if (audio_len == 0)
            return;
        len = (len > audio_len ? audio_len : len);	/*  Mix  as  much  data  as  possible  */
        SDL_MixAudio(stream, audio_pos, len, SDL_MIX_MAXVOLUME);
        audio_pos += len;
        audio_len -= len;
    }
#pragma endregion _AUDIO


    PCMPlayer::PCMPlayer()
    {
    }


    PCMPlayer::~PCMPlayer()
    {
    }


    int  PCMPlayer::PlayPCM(const string & tFileName)
    {
        if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_TIMER)) {
            printf("Could not initialize SDL - %s\n", SDL_GetError());
            return -1;
        }
        //SDL_AudioSpec
        SDL_AudioSpec wanted_spec;
        wanted_spec.freq = 44100;
        wanted_spec.format = AUDIO_S16SYS;
        wanted_spec.channels = 2;
        wanted_spec.silence = 0;
        wanted_spec.samples = 1024;
        wanted_spec.callback = fill_audio;

        if (SDL_OpenAudio(&wanted_spec, NULL) < 0) {
            printf("can't open audio.\n");
            return -1;
        }

        FILE *fp = fopen(tFileName.c_str(), "rb+");
        if (fp == NULL) {
            printf("cannot open this file\n");
            return -1;
        }

        int pcm_buffer_size = 4096;
        char *pcm_buffer = (char *)malloc(pcm_buffer_size);
        int data_count = 0;

        //Play
        SDL_PauseAudio(0);

        while (1) {
            if (fread(pcm_buffer, 1, pcm_buffer_size, fp) != pcm_buffer_size) {
                // Loop
                fseek(fp, 0, SEEK_SET);
                fread(pcm_buffer, 1, pcm_buffer_size, fp);
                data_count = 0;
            }
            printf("Now Playing %10d Bytes data.\n", data_count);
            data_count += pcm_buffer_size;
            //Set audio buffer (PCM data)
            audio_chunk = (Uint8 *)pcm_buffer;
            //Audio buffer length
            audio_len = pcm_buffer_size;
            audio_pos = audio_chunk;

            while (audio_len > 0)//Wait until finish
                SDL_Delay(1);
        }
        free(pcm_buffer);
        SDL_Quit();
    }






#pragma region  _VIDEO

    const int bpp = 12;

    int screen_w = 512, screen_h = 288;
    const int pixel_w = 512, pixel_h = 288;
    unsigned char *buffer = new unsigned char(pixel_w*pixel_h*bpp / 8);


    //Refresh Event
#define REFRESH_EVENT  (SDL_USEREVENT + 1)

#define BREAK_EVENT  (SDL_USEREVENT + 2)

    int thread_exit = 0;

    int refresh_video(void *opaque) {
        thread_exit = 0;
        while (!thread_exit) {
            SDL_Event event;
            event.type = REFRESH_EVENT;
            SDL_PushEvent(&event);
            SDL_Delay(40);
        }
        thread_exit = 0;
        //Break
        SDL_Event event;
        event.type = BREAK_EVENT;
        SDL_PushEvent(&event);

        return 0;
    }
#pragma endregion  _VIDEO


    YUVPlayer::YUVPlayer()
    {

    }

    YUVPlayer::~YUVPlayer()
    {

    }

    int YUVPlayer::PlayYUV(const std::string& tFileName, int tWidth, int tHeight)
    {
        if (SDL_Init(SDL_INIT_VIDEO)) {
            printf("Could not initialize SDL - %s\n", SDL_GetError());
            return -1;
        }

        SDL_Window *screen;

        //SDL 2.0 Support for multiple windows
        screen = SDL_CreateWindow("Simplest Video Play SDL2", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            screen_w, screen_h, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
        if (!screen) {
            printf("SDL: could not create window - exiting:%s\n", SDL_GetError());
            return -1;
        }
        SDL_Renderer* sdlRenderer = SDL_CreateRenderer(screen, -1, 0);

        Uint32 pixformat = 0;

        //IYUV: Y + U + V  (3 planes)
        //YV12: Y + V + U  (3 planes)
        pixformat = SDL_PIXELFORMAT_IYUV;

        SDL_Texture* sdlTexture = SDL_CreateTexture(sdlRenderer, pixformat, SDL_TEXTUREACCESS_STREAMING, tWidth, tHeight);

        FILE *fp = NULL;
        fp = fopen(tFileName.c_str(), "rb+");

        if (fp == NULL) {
            printf("cannot open this file\n");
            return -1;
        }

        SDL_Rect sdlRect;
        SDL_Rect sdlRect1;
        SDL_Rect sdlRect2;
        SDL_Rect sdlRect3;

        SDL_Thread *refresh_thread = SDL_CreateThread(refresh_video, NULL, NULL);
        SDL_Event event;
        while (1) {
            //Wait
            SDL_WaitEvent(&event);
            if (event.type == REFRESH_EVENT) {
                if (fread(buffer, 1, tWidth*tHeight*bpp / 8, fp) != tWidth*tHeight*bpp / 8) {
                    // Loop
                    fseek(fp, 0, SEEK_SET);
                    fread(buffer, 1, tWidth*tHeight*bpp / 8, fp);
                }

                SDL_UpdateTexture(sdlTexture, NULL, buffer, tWidth);

                //FIX: If window is resize
                sdlRect.x = 0;
                sdlRect.y = 0;
                sdlRect.w = screen_w;
                sdlRect.h = screen_h / 2;

                sdlRect1.x = 0;
                sdlRect1.y = screen_h / 2;
                sdlRect1.w = screen_w;
                sdlRect1.h = screen_h;

                //sdlRect2.x = 0;
                //sdlRect2.y = screen_h / 2;
                //sdlRect2.w = screen_w;
                //sdlRect2.h = screen_h;

                //sdlRect3.x = screen_w / 2;
                //sdlRect3.y = screen_h / 2;
                //sdlRect3.w = screen_w;
                //sdlRect3.h = screen_h;

                SDL_RenderClear(sdlRenderer);
                SDL_RenderCopy(sdlRenderer, sdlTexture, NULL, &sdlRect);

                SDL_RenderCopy(sdlRenderer, sdlTexture, NULL, &sdlRect1);
                //SDL_RenderCopy(sdlRenderer, sdlTexture, NULL, &sdlRect2);
                //SDL_RenderCopy(sdlRenderer, sdlTexture, NULL, &sdlRect3);
                SDL_RenderPresent(sdlRenderer);

            }
            else if (event.type == SDL_WINDOWEVENT) {
                //If Resize
                SDL_GetWindowSize(screen, &screen_w, &screen_h);
            }
            else if (event.type == SDL_QUIT) {
                thread_exit = 1;
            }
            else if (event.type == BREAK_EVENT) {
                break;
            }
        }
        SDL_Quit();
        return 0;

    }


}