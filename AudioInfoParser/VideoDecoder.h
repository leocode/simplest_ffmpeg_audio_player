#pragma once
#include <string>


namespace Decoder
{


    class VideoDecoder
    {
        typedef unsigned char uint8;
    public:
        VideoDecoder();
        ~VideoDecoder();
        int  Converter2YUV(const std::string& tFilename);
        //bool SaveYUVFile(const std::string& tFileName);
    private:
        std::string m_YUV420PBuff;
    };

}