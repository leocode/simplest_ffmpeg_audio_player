#pragma once



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#define __STDC_CONSTANT_MACROS

//Windows
extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
};

long GetAudioTimeLenth(const std::string & tFileName)
{
    if (tFileName.empty())
    {
        return 0;
    }
    AVFormatContext	*pFormatCtx;
    bool bSuccess = true;
    //char url[] = "xiaoqingge.mp3";
    long iDuration = 0;

    av_register_all();
    avformat_network_init();
    pFormatCtx = avformat_alloc_context();
    //Open
    if (avformat_open_input(&pFormatCtx, tFileName.c_str(), NULL, NULL) != 0) {
        printf("Couldn't open input stream.\n");
        bSuccess = false;
    }
    // Retrieve stream information
    if (bSuccess && avformat_find_stream_info(pFormatCtx, NULL) < 0) {
        printf("Couldn't find stream information.\n");
        bSuccess = false;
    }
    if (bSuccess)
    {
        iDuration =(long)pFormatCtx->duration /1000;  // transfer into ms unit
    }
    avformat_close_input(&pFormatCtx);
    return iDuration;
}