#pragma once
#include "MediaData.h"




struct RIFF_HEADER
{
    char szRiffID[4];  // 'R','I','F','F'
    unit64 dwRiffSize; //该size是整个wav文件大小减去ID
    char szRiffFormat[4]; // 'W','A','V','E'
};


struct WAVE_FORMAT
{
    uint16 wFormatTag; //编码方式，一般为0x0001
    uint16 wChannels; //声道数目，1--单声道；2--双声道 
    unit64 dwSamplesPerSec; //采样频率 
    unit64 dwAvgBytesPerSec; //每秒所需字节数
    uint16 wBlockAlign;  //数据块对齐单位(每个采样需要的字节数
    uint16 wBitsPerSample; // 每个采样需要的bit数
};

struct FMT_BLOCK
{
    char  szFmtID[4]; // 'f','m','t',' '
    unit64  dwFmtSize;
    WAVE_FORMAT wavFormat;
};

struct FACT_BLOCK
{
    char  szFactID[4]; // 'f','a','c','t'
    unit64  dwFactSize;
};


struct DATA_BLOCK
{
    char szDataID[4]; // 'd','a','t','a'
    unit64 dwDataSize;
};




class PCM2Wav
{
public:
    PCM2Wav();
    ~PCM2Wav();
};

