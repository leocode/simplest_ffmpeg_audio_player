#pragma once
#include <string>
using namespace  std;


namespace Player
{

    class PCMPlayer
    {
    public:
        PCMPlayer();
        ~PCMPlayer();
        static int  PlayPCM(const string & tFileName);
    };

    class YUVPlayer
    {
    public:
        YUVPlayer();
        ~YUVPlayer();
        static int PlayYUV(const std::string& tFileName, int tWidth, int tHeight);
    };
}