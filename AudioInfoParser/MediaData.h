#pragma once

typedef short int16;
typedef unsigned int uint;
typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned short ushort;
typedef unsigned char uchar;
typedef unsigned int uint32;
typedef unsigned long  unit64;


#pragma pack(push, 1)


struct Media_HEADER {
    uint16 format; 			//2 bytes  0x0001          Format tag: 1 = PCM
    uint16 channels; 		//2 bytes  <channels>      Channels: 1 = mono, 2 = stereo
    uint32 sampleRate;		//4 bytes  <sample rate>   Samples per second: e.g., 44100
    uint32 byteRate;		//4 bytes  <bytes/second>  sample rate * block align
    uint16 blockAlign;		//2 bytes  <block align>   channels * bits/sample / 8
    uint16 bitsPerSample; 	//2 bytes  <bits/sample>   8 or 16
    uint16 extraBytes;		// for compatible with WAVEFORMATEX struct 
};



#pragma pack(pop)
