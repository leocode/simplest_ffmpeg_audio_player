#pragma once
#include <string>

class YUV420Convertor
{
public:

	static int Convertor_2(const std::string & tSrcFile, const std::string & tDestDir, int tWidth, int tHeight);

private:
	static void ConvertorBMP(const std::string tSrcFile, const std::string & tDestDir, const int& tWidth, const int& tHeight);

};

