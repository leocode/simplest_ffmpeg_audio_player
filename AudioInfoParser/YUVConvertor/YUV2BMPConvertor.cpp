#include "stdafx.h"
#include "YUV2BMPConvertor.h"
#include "bmp_utils.h"

#include "assert.h"
#include <stdio.h>
extern "C"
{
#include "libswscale/swscale.h"
#include "libavutil/opt.h"
#include "libavutil/imgutils.h"
};




int YUV420Convertor::Convertor_2(const std::string & tSrcFile, const std::string & tDestDir, int tWidth, int tHeight)
{
	FILE *src_file = fopen(tSrcFile.c_str(), "rb");

	//const int tWidth = 480, tHeight = 272;
	AVPixelFormat src_pixfmt = AV_PIX_FMT_YUV420P;

	int src_bpp = av_get_bits_per_pixel(av_pix_fmt_desc_get(src_pixfmt));

	char  iFile[128] = { 0 };
	sprintf(iFile, "%s/temp.rgb", tDestDir.c_str());

	FILE *dst_file = fopen(iFile, "wb");
	//const int tWidth = 640, tHeight = 480;
	AVPixelFormat dst_pixfmt = AV_PIX_FMT_RGB24;
	int dst_bpp = av_get_bits_per_pixel(av_pix_fmt_desc_get(dst_pixfmt));

	//Structures
	uint8_t *src_data[4];
	int src_linesize[4];

	uint8_t *dst_data[4];
	int dst_linesize[4];

	int rescale_method = SWS_BICUBIC;
	struct SwsContext *img_convert_ctx;
	uint8_t *temp_buffer = (uint8_t *)malloc(tWidth*tHeight*src_bpp / 8);

	int frame_idx = 0;
	int ret = 0;
	ret = av_image_alloc(src_data, src_linesize, tWidth, tHeight, src_pixfmt, 1);
	if (ret< 0) {
		printf("Could not allocate source image\n");
		return -1;
	}
	ret = av_image_alloc(dst_data, dst_linesize, tWidth, tHeight, dst_pixfmt, 1);
	if (ret< 0) {
		printf("Could not allocate destination image\n");
		return -1;
	}
	//----------------------------- 
	//Init Method 1
	img_convert_ctx = sws_alloc_context();
	//Show AVOption
	av_opt_show2(img_convert_ctx, stdout, AV_OPT_FLAG_VIDEO_PARAM, NULL);
	//Set Value
	av_opt_set_int(img_convert_ctx, "sws_flags", SWS_BICUBIC | SWS_PRINT_INFO, NULL);
	av_opt_set_int(img_convert_ctx, "srcw", tWidth, NULL);
	av_opt_set_int(img_convert_ctx, "srch", tHeight, NULL);
	av_opt_set_int(img_convert_ctx, "src_format", src_pixfmt, NULL);
	//'0' for MPEG (Y:0-235);'1' for JPEG (Y:0-255)
	av_opt_set_int(img_convert_ctx, "src_range", 1, NULL);
	av_opt_set_int(img_convert_ctx, "dstw", tWidth, NULL);
	av_opt_set_int(img_convert_ctx, "dsth", tHeight, NULL);
	av_opt_set_int(img_convert_ctx, "dst_format", dst_pixfmt, NULL);
	av_opt_set_int(img_convert_ctx, "dst_range", 1, NULL);
	sws_init_context(img_convert_ctx, NULL, NULL);

	//Init Method 2
	//img_convert_ctx = sws_getContext(src_w, src_h,src_pixfmt, dst_w, dst_h, dst_pixfmt, 
	//  rescale_method, NULL, NULL, NULL); 
	//-----------------------------
	/*
	//Colorspace
	ret=sws_setColorspaceDetails(img_convert_ctx,sws_getCoefficients(SWS_CS_ITU601),0,
	sws_getCoefficients(SWS_CS_ITU709),0,
	0, 1 << 16, 1 << 16);
	if (ret==-1) {
	printf( "Colorspace not support.\n");
	return -1;
	}
	*/
	while (1)
	{
		if (fread(temp_buffer, 1, tWidth*tHeight*src_bpp / 8, src_file) != tWidth*tHeight*src_bpp / 8) {
			break;
		}

		switch (src_pixfmt) {
		case AV_PIX_FMT_GRAY8: {
			memcpy(src_data[0], temp_buffer, tWidth*tHeight);
			break;
		}
		case AV_PIX_FMT_YUV420P: {
			memcpy(src_data[0], temp_buffer, tWidth*tHeight);                    //Y
			memcpy(src_data[1], temp_buffer + tWidth*tHeight, tWidth*tHeight / 4);      //U
			memcpy(src_data[2], temp_buffer + tWidth*tHeight * 5 / 4, tWidth*tHeight / 4);  //V
			break;
		}
		case AV_PIX_FMT_YUV422P: {
			memcpy(src_data[0], temp_buffer, tWidth*tHeight);                    //Y
			memcpy(src_data[1], temp_buffer + tWidth*tHeight, tWidth*tHeight / 2);      //U
			memcpy(src_data[2], temp_buffer + tWidth*tHeight * 3 / 2, tWidth*tHeight / 2);  //V
			break;
		}
		case AV_PIX_FMT_YUV444P: {
			memcpy(src_data[0], temp_buffer, tWidth*tHeight);                    //Y
			memcpy(src_data[1], temp_buffer + tWidth*tHeight, tWidth*tHeight);        //U
			memcpy(src_data[2], temp_buffer + tWidth*tHeight * 2, tWidth*tHeight);      //V
			break;
		}
		case AV_PIX_FMT_YUYV422: {
			memcpy(src_data[0], temp_buffer, tWidth*tHeight * 2);                  //Packed
			break;
		}
		case AV_PIX_FMT_RGB24: {
			memcpy(src_data[0], temp_buffer, tWidth*tHeight * 3);                  //Packed
			break;
		}
		default: {
			printf("Not Support Input Pixel Format.\n");
			break;
		}
		}

		sws_scale(img_convert_ctx, src_data, src_linesize, 0, tHeight, dst_data, dst_linesize);
		printf("Finish process frame %5d\n", frame_idx);
		frame_idx++;

		switch (dst_pixfmt) {
		case AV_PIX_FMT_GRAY8: {
			fwrite(dst_data[0], 1, tWidth*tHeight, dst_file);
			break;
		}
		case AV_PIX_FMT_YUV420P: {
			fwrite(dst_data[0], 1, tWidth*tHeight, dst_file);                 //Y
			fwrite(dst_data[1], 1, tWidth*tHeight / 4, dst_file);               //U
			fwrite(dst_data[2], 1, tWidth*tHeight / 4, dst_file);               //V
			break;
		}
		case AV_PIX_FMT_YUV422P: {
			fwrite(dst_data[0], 1, tWidth*tHeight, dst_file);                 //Y
			fwrite(dst_data[1], 1, tWidth*tHeight / 2, dst_file);               //U
			fwrite(dst_data[2], 1, tWidth*tHeight / 2, dst_file);               //V
			break;
		}
		case AV_PIX_FMT_YUV444P: {
			fwrite(dst_data[0], 1, tWidth*tHeight, dst_file);                 //Y
			fwrite(dst_data[1], 1, tWidth*tHeight, dst_file);                 //U
			fwrite(dst_data[2], 1, tWidth*tHeight, dst_file);                 //V
			break;
		}
		case AV_PIX_FMT_YUYV422: {
			fwrite(dst_data[0], 1, tWidth*tHeight * 2, dst_file);               //Packed
			break;
		}
		case AV_PIX_FMT_RGB24: {
			fwrite(dst_data[0], 1, tWidth*tHeight * 3, dst_file);               //Packed
			break;
		}
		default: {
			printf("Not Support Output Pixel Format.\n");
			break;
		}
		}

	}

	sws_freeContext(img_convert_ctx);

	free(temp_buffer);
	fclose(dst_file);
	av_freep(&src_data[0]);
	av_freep(&dst_data[0]);

	YUV420Convertor::ConvertorBMP(iFile, tDestDir, tWidth, tHeight);

}

void YUV420Convertor::ConvertorBMP(const std::string tSrcFile, const std::string & tDestDir, const int& tWidth, const int& tHeight)
{


	FILE* fp1;
	int frameSize = 0;
	int picSize = 0;
	int ret = 1;
	unsigned char* framePtr = NULL;
	unsigned char* rgbPtr = NULL;
	long rgbSize = 0;

	rgbSize = tWidth * tHeight * 3;

	rgbPtr = (unsigned char *)malloc(sizeof(unsigned char) * rgbSize);

	memset(rgbPtr, '\0', rgbSize);

	if ((fp1 = fopen(tSrcFile.c_str(), "rb")) == NULL)
	{
		printf("open yuv file failed.\n");
		return ;
	}

	int iIndex = 1;
	while (ret > 0)
	{
		ret = (int)fread(rgbPtr, 1, rgbSize, fp1);
		//yuv420p_to_rgb24(framePtr, rgbPtr, tWidth, tHeight);
		//yuv_to_rgb24(YUV420P, framePtr, rgbPtr, tWidth, tHeight);
		//yuv420_to_rgb24_2(framePtr, rgbPtr, tWidth, tHeight);
		// rgb --> bgr
		// save file
		char  iFile[128] = { 0 };
		sprintf(iFile, "%s/pic_%d.bmp",tDestDir.c_str(), iIndex++);
		swap_rgb(rgbPtr, rgbSize);
		write_bmp_file(iFile, rgbPtr, tWidth, tHeight);
	}
	fclose(fp1);
	free(rgbPtr);

}

